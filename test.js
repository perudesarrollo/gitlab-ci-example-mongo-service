const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

let host = process.env.DB_HOST ? process.env.DB_HOST : 'localhost';
let port = process.env.DB_PORT ? process.env.DB_PORT : '27017';
let dbName = process.env.DB_NAME ? process.env.DB_NAME : 'reach-engine';
let collectionName = process.env.DB_COLLECTION ? process.env.DB_COLLECTION : 'MyDummyCollection';

// Connection URL
const url = 'mongodb://' + host + ':' + port;

// Use connect method to connect to the server
MongoClient.connect(url, function (err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");

    const db = client.db(dbName);
    console.log("database:");
    console.log(db);

    const collection = db.collection(collectionName);
    collection.find({}).toArray(function (err, docs) {
        assert.equal(err, null);
        console.log("Found the following records");
        console.log(docs);
        assert('Jen Ford', docs[0].name)
    });

    client.close();
});

